Rails.application.routes.draw do

  resources :accounts do
    resources :transaccions
    # Hack feo por error de tipeo
    resources :transaccions, :path => 'transactions', :as => 'transactions'
  end

  get 'accounts/:id/categories', to: 'accounts#show_categories', as:'account_categories'
  root 'accounts#index'

  resources :categories

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
