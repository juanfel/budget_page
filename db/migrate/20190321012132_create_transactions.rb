class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string :name
      t.integer :amount
      t.date :transaction_date
      t.text :comments
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
