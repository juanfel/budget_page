class CreateCategoriestransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :category_transactions do |t|
      t.belongs_to :category, index: true
      t.belongs_to :transaction, index: true
      t.timestamps
    end
  end
end
