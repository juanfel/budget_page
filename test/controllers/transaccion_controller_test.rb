require 'test_helper'

class TransaccionControllerTest < ActionDispatch::IntegrationTest
  test "should get get" do
    get transaccion_get_url
    assert_response :success
  end

  test "should get create" do
    get transaccion_create_url
    assert_response :success
  end

  test "should get delete" do
    get transaccion_delete_url
    assert_response :success
  end

end
