class CategoryTransaction < ApplicationRecord
  belongs_to :category, optional: true
  belongs_to :my_transaction, class_name: 'Transaction', optional: true
end
