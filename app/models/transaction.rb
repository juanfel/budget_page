class Transaction < ApplicationRecord
  belongs_to :account
  has_many :category_transaction
  has_many :categories, through: :category_transaction
end
