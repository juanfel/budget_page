class Category < ApplicationRecord
  has_many :category_transaction
  has_many :my_transaction, class_name: 'Transaction', through: :category_transaction
end
