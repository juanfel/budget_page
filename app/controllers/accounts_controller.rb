class AccountsController < ApplicationController
  def new
  end

  def create
    @account = Account.new(account_params)

    if @account.save
      redirect_to @account
    else
      render 'new'
    end
  end

  def index
    @accounts = Account.all
  end

  def show
    @account = Account.find(params[:id])
    @total_amount = @account.transactions.sum('amount')
    # Se separan en la parte que suma a la cuenta y en la que resta.
    @debe = @account.transactions.where('amount < 0').sum('amount')
    @haber = @total_amount - @debe # Notese que debe es negativo, por lo cual se despeja restando (T = H + D)
  end

  def show_categories
    @account = Account.find(params[:id])

    @resultado = {}

    @joined_view = @account.transactions.joins(:categories)
    Rails.logger.debug(@joined_view.inspect)
    Category.find_each do |category|
      @amount = @joined_view.where(categories: {id: category.id}).sum('amount')
      Rails.logger.debug(@amount.inspect)
      @resultado[category.name] = @amount unless @amount.zero?
    end
  end

  private

  def account_params
    params.require(:account).permit(:name, :id)
  end

end
