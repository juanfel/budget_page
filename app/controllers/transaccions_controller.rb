class TransaccionsController < ApplicationController
  def get
  end

  def new
    @account = Account.find(params[:account_id])
    @transaccion = @account.transactions.build
    @categories = Category.all
  end

  def edit
    @account = Account.find(params[:account_id])
    @transaccion = @account.transactions.find(params[:id])
  end

  def update
    @account = Account.find(params[:account_id])
    @transaccion = @account.transactions.find(params[:id])
    @categories = Category.all

    # No quiero que se agreguen tags infinitamente.
    @transaccion.categories.clear
    @categories = Category.find(category_param[:category_id])

    @categories.each do |category|
      @transaccion.categories << category
    end

    Rails.logger.info(@transaccion.errors.inspect)

    if @transaccion.update(transaccion_params)
      redirect_to account_path(@account)
    else
      render 'edit'
    end
  end
  def create
    unless params[:transaction].nil? or params[:account_id].nil?
      @account = Account.find(params[:account_id])
      @transaccion = @account.transactions.create(transaccion_params)

      @categories = Category.find(category_param[:category_id])

      @categories.each do |category|
        @transaccion.categories << category
      end

      if @transaccion.save
        redirect_to account_path(@account)
      else
        render 'new'
      end

    else
      render 'new'
    end
  end

  def destroy
    @account = Account.find(params[:account_id])
    @transaction = @account.transactions.find(params[:id])
    @transaction.destroy
    redirect_to account_path(@account)
  end

  def show
    @account = Account.find(params[:account_id])
    @transaction = @account.transactions.find(params[:id])
    @categories = @transaction.categories
  end

  private

  def transaccion_params
    params.require(:transaction).permit(:name,
                                        :amount,
                                        :transaction_date,
                                        :account_id,
                                        :comments).except(:category_id)
  end

  def category_param
    params.require(:transaction)
  end
end
